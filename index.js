const path = require('path')
const _ = require('lodash');

const MySQLPrinter = require('./src/mysql-printer')

require('dotenv').config()

const env_vars = {
  MYSQL_HOST: process.env.MYSQL_HOST,
  MYSQL_USER: process.env.MYSQL_USER,
  MYSQL_PASSWORD: process.env.MYSQL_PASSWORD,
  MYSQL_DATABASE: process.env.MYSQL_DATABASE,
  MYSQL_TABLE: process.env.MYSQL_TABLE,
}

let enabled = true

for (const env_var in env_vars) {
  if (env_vars[env_var] === undefined) {
    console.error(`Environment variable ${env_var} is not set - must be set to send results to MySQL`)
    enabled = false
  }
}

if (!enabled) {
  process.exit(1)
}

function getFormattedDate(timestamp) {
  const date = new Date(timestamp)

  let month = date.getMonth() + 1
  let day = date.getDate()
  let hour = date.getHours()
  let min = date.getMinutes()

  month = (month < 10 ? "0" : "") + month
  day = (day < 10 ? "0" : "") + day
  hour = (hour < 10 ? "0" : "") + hour
  min = (min < 10 ? "0" : "") + min

  const str = date.getFullYear() + "-" + month + "-" + day + "-" +  hour + ":" + min
  return str
}

/**
 * Intercepts test results and sends them to SQL Database
 * The MYSQL_HOST must be set to use this
 */
const MySQLPlugin = {
  onTestSuiteStart: async(testSuite) => {
    this.runId = testSuite.runId
    this.runTimestamp = testSuite.runTimestamp ? getFormattedDate(testSuite.runTimestamp) : getFormattedDate(Date.now())
  },
  onTestEnd: async(test, testResult) => {
    try {
      const rows = []
      const timestamp = Date.now()
      const test_suite_path = path.basename(test.testSuite.fileName)
      const tags = _.get(test, 'tags', []).join(', ')
      const test_success = testResult.passed
      const runId = this.runId
      const runTimestamp = this.runTimestamp
  
      testResult._interactionResults.forEach((interaction, index) => {
        const row = {}
        const test_name = _.get(interaction, 'interactionDto.testDescription')
        const assertionValue = _.get(interaction, 'interactionDto.assertions[0].value')
        let expectedValues = ''
        if (_.isArray(assertionValue)) {
          expectedValues = assertionValue.join('|')
        } else if (typeof assertionValue === 'string') {
          expectedValues = assertionValue
        } 
        row['id'] = `${test_suite_path}_${test_name}_${timestamp}_${index}`
        row['test_suite_name'] = _.get(test, 'testSuite.description', test_suite_path)
        row['run_id'] = runId
        row['run_timestamp'] = runTimestamp
  
        row['timestamp'] = Date.now()
        row['test_name'] = test_name
        row['test_success'] = test_success
        row['utterance'] = _.get(interaction, 'interactionDto.utterance')
        row['expected_values'] = expectedValues
        row['actual_transcript'] = _.get(interaction, '_rawResponse.transcript')
        row['utterance_success'] = _.get(interaction, 'interactionDto.result.passed')
        row['tags'] = tags
        row['caption'] = _.get(interaction, '_rawResponse.caption')
        row['display'] = _.get(interaction, '_rawResponse.display')
        row['raw'] = _.get(interaction, 'interactionDto.result.rawResponse')
  
        row['voice_id'] = _.get(testResult, 'test.testSuite.voiceId', 'default').toString()
        row['locale'] = _.get(testResult, '_locale') || _.get(test, '_testSuite._configuration.locale', 'default')
        row['utterance_error'] = _.get(interaction, 'errors', null)
        
        rows.push(row)
      })
      const mysqlPrinter = new MySQLPrinter()
      await mysqlPrinter.print(rows)
    } catch (error) {
      console.error(error.message)
    }

  },
}

module.exports = MySQLPlugin
