# **Bespoken MySQL Plugin**
This plugin makes it easy to send your voice app's end-to-end test results to a sql instance for reporting and monitoring.

It leverages Bespoken's filters to report test results to MySQL.

## **Getting Started**
### **Installation and Usage**
To use the Bespoken MySQL Plugin, just include it in your test project dependencies with:
```
npm install bespoken-mysql-plugin --save
```

Then include it as your filter like this in the testing.json:
```
{
  "filter": "./node_modules/bespoken-mysql-plugin/index.js"
}
```

### **Environment Variables**

| Variable |
| --- |
| MYSQL_HOST |
| MYSQL_USER |
| MYSQL_PASSWORD |
| MYSQL_DATABASE |
| MYSQL_TABLE |

For local running, we typically recommend using the package `dotenv` and setting it in a file labeled `.env`. We have provided a file example.env you can use as a template (just copy and rename to .env).

It can also be set manually like so:
```bash
export MYSQL_HOST=<MYSQL_HOST>
```
(Use `set` instead of `export` if using Windows Command Prompt).

### **SQL Columns**
By default we report the results at utterance level:

| Columns |
| --- |
| ID |
| TEST_SUITE_NAME |
| TIMESTAMP |
| TEST_NAME |
| TEST_SUCCESS |
| UTTERANCE |
| EXPECTED_VALUES |
| ACTUAL_TRANSCRIPT |
| UTTERANCE_SUCCESS |
| TAGS |
| CAPTION |
| DISPLAY |
| RAW |
| VOICE_ID |
| LOCALE |
| UTTERANCE_ERROR |
